<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Captura extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Captura_model");
	}

	public function Index()
	{
		$data = array(
			'capturacali' => $this->Captura_model->getregistro()
			,);
		
		
            $this->load->view("Layouts/Header");
            $this->load->view("Layouts/Aside");
            $this->load->view("Admin/Captura/Registrar",$data);
            $this->load->view("Layouts/Footer");
	}

	public function Add()
	{
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Captura/Add");
		$this->load->view("Layouts/Footer");
	}

	public function Store(){
		$Matricula = $this ->input->post("Matricula");
		$Nombre = $this ->input->post("Nombre");
		$Parcial1 = $this ->input->post("Parcial1");
		$Parcial2= $this ->input->post("Parcial2");
		$Parcial3 = $this ->input->post("Parcial3");
		$Parcial4 = $this ->input->post("Parcial4");
		$Parcial5 = $this ->input->post("Parcial5");
		$Parcial6 = $this ->input->post("Parcial6");
		$Parcial7 = $this ->input->post("Parcial7");
        $Parcial8 = $this ->input->post("Parcial8");
        $Parcial9 = $this ->input->post("Parcial9");
       
        
		
		
		$data  =array(
			'Matricula' => $Matricula,
			'Nombre' => $Nombre,
			'Parcial1' => $Parcial1,
			'Parcial2' => $Parcial2,
			'Parcial3' => $Parcial3,
			'Parcial4' => $Parcial4,
			'Parcial5' => $Parcial5,
            'Parcial6' => $Parcial6,
            'Parcial7' => $Parcial7,
            'Parcial8' => $Parcial8,
            'Parcial9' => $Parcial9,
            'Estado' => "1"
		);

	if ($this->Captura_model->save($data)) {
		redirect(base_url()."Captura/Captura");

	}else
	$this->session->ser_flashdata("error","no se relizaron registros");
	redirect(base_url()."Captura/Captura/Add");

	}

	public function Edit($id){

		$data = array(
			'capturacali' => $this->Captura_model->getregistros($id),);
		$this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Captura/Edit",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Update(){
		$id= $this ->input->post("id");
		$Matricula = $this ->input->post("Matricula");
		$Nombre = $this ->input->post("Nombre");
		$Parcial1 = $this ->input->post("Parcial1");
		$Parcial2= $this ->input->post("Parcial2");
		$Parcial3 = $this ->input->post("Parcial3");
		$Parcial4 = $this ->input->post("Parcial4");
		$Parcial5 = $this ->input->post("Parcial5");
		$Parcial6 = $this ->input->post("Parcial6");
		$Parcial7 = $this ->input->post("Parcial7");
        $Parcial8 = $this ->input->post("Parcial8");
        $Parcial9 = $this ->input->post("Parcial9");

		$data  =array(
			'Matricula' => $Matricula,
			'Nombre' => $Nombre,
			'Parcial1' => $Parcial1,
			'Parcial2' => $Parcial2,
			'Parcial3' => $Parcial3,
			'Parcial4' => $Parcial4,
			'Parcial5' => $Parcial5,
            'Parcial6' => $Parcial6,
            'Parcial7' => $Parcial7,
            'Parcial8' => $Parcial8,
            'Parcial9' => $Parcial9            
		);
		if ($this->Captura_model->Update($id,$data)) {
			redirect(base_url()."Captura/Captura");
		}else{
			$this->session->ser_flashdata("error","no se actualizaron los campos");
				redirect(base_url()."Captura/Captura/Add");
		}
	}

	public function Delete($id){
		$data  =array(
			'Estado' => "0",
		);
		$this->Captura_model->Update($id,$data);
		
		redirect(base_url()."Captura/Captura");
	}
}
