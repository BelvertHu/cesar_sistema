<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registros extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Registro_model");
	}

	public function Index()
	{
		$data = array(
			'persona_docentes' => $this->Registro_model->getregistro()
			,);
		
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Contenido/Registrar",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Add()
	{
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Contenido/Add");
		$this->load->view("Layouts/Footer");
	}

	public function Store(){
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoP");
		$Ciudad = $this ->input->post("Ciudad");
		$direccion = $this ->input->post("direccion");
		$telefono = $this ->input->post("telefono");
		$fechana = $this ->input->post("fechana");
		$sexo = $this ->input->post("sexo");
		$Tipo = $this ->input->post("Tipo");
		$matricula = $this ->input->post("matricula");
		
		
		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'Ciudad' => $Ciudad,
			'direccion' => $direccion,
			'telefono' => $telefono,
			'fechana' => $fechana,
			'sexo' => $sexo,
			'Tipo' => $Tipo,
			'matricula' => $matricula,
			'estado' => "1"
		);

	if ($this->Registro_model->save($data)) {
		redirect(base_url()."Registros/Registros");

	}else
	$this->session->ser_flashdata("error","no se relizaron registros");
	redirect(base_url()."Registros/Registros/Add");

	}

	public function Edit($id_persona_docente){

		$data = array(
			'persona_docentes' => $this->Registro_model->getregistros($id_persona_docente),);
		$this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Contenido/Edit",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Update(){
		$id_persona_docente= $this ->input->post("ideditr");
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoM");
		$Ciudad = $this ->input->post("Ciudad");
		$direccion = $this ->input->post("direccion");
		$telefono = $this ->input->post("telefono");
		$fechana = $this ->input->post("fechana");
		$sexo = $this ->input->post("sexo");
		$Tipo = $this ->input->post("Tipo");
		$matricula = $this ->input->post("matricula");

		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'Ciudad' => $Ciudad,
			'direccion' => $direccion,
			'telefono' => $telefono,
			'fechana' => $fechana,
			'sexo' => $sexo,
			'Tipo' => $Tipo,
			'matricula' => $matricula
		);
		if ($this->Registro_model->Update($id_persona_docente,$data)) {
			redirect(base_url()."Registros/Registros");
		}else{
			$this->session->ser_flashdata("error","no se actualizaron los campos");
				redirect(base_url()."Registros/Registros/Add");
		}
	}

	public function Delete($id_persona_docente){
		$data  =array(
			'estado' => "0",
		);
		$this->Registro_model->Update($id_persona_docente,$data);
		
		redirect(base_url()."Registros/Registros");
	}
}
