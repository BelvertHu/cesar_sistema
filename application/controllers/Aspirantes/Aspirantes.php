<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspirantes extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Aspirante_model");
	}

	public function Index()
	{
		$data = array(
			'aspirantes' => $this->Aspirante_model->getregistro()
			,);
		
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes/Registrar",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Add()
	{
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes/Add");
		$this->load->view("Layouts/Footer");
	}

	public function Store(){
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoM");
		$FNacimiento = $this ->input->post("FNacimiento");
		$curp = $this ->input->post("curp");
		$sexo = $this ->input->post("sexo");
		$estado = $this ->input->post("estado");
		$municipio = $this ->input->post("municipio");
		$localidad = $this ->input->post("localidad");
        $CP = $this ->input->post("CP");
        $NumeroDomicilio = $this ->input->post("NumeroDomicilio");
        $Telefefono = $this ->input->post("Telefefono");
        $Telefono2 = $this ->input->post("Telefono2");
        $codigo = $this ->input->post("codigo");
        
		
		
		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'FNacimiento' => $FNacimiento,
			'curp' => $curp,
			'sexo' => $sexo,
			'estado' => $estado,
			'municipio' => $municipio,
            'localidad' => $localidad,
            'CP' => $CP,
            'NumeroDomicilio' => $NumeroDomicilio,
            'Telefefono' => $Telefefono,
            'Telefono2' => $Telefono2,
            'codigo' => $codigo,
            'Aceptados' => "1"
		);

	if ($this->Aspirante_model->save($data)) {
		redirect(base_url()."Aspirantes/Aspirantes");

	}else
	$this->session->ser_flashdata("error","no se relizaron registros");
	redirect(base_url()."Aspirantes/Aspirantes/Add");

	}

	public function Edit($idAspirantes){

		$data = array(
			'aspirantes' => $this->Aspirante_model->getregistros($idAspirantes),);
		$this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes/Edit",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Update(){
		$idAspirantes= $this ->input->post("idAspirantes");
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoM");
		$FNacimiento = $this ->input->post("FNacimiento");
		$curp = $this ->input->post("curp");
		$sexo = $this ->input->post("sexo");
		$estado = $this ->input->post("estado");
		$municipio = $this ->input->post("municipio");
		$localidad = $this ->input->post("localidad");
        $CP = $this ->input->post("CP");
        $NumeroDomicilio = $this ->input->post("NumeroDomicilio");
        $Telefefono = $this ->input->post("Telefefono");
        $Telefono2 = $this ->input->post("Telefono2");
        $codigo = $this ->input->post("codigo");

		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'FNacimiento' => $FNacimiento,
			'curp' => $curp,
			'sexo' => $sexo,
			'estado' => $estado,
			'municipio' => $municipio,
            'localidad' => $localidad,
            'CP' => $CP,
            'NumeroDomicilio' => $NumeroDomicilio,
            'Telefefono' => $Telefefono,
            'Telefono2' => $Telefono2,
            'codigo' => $codigo
		);
		if ($this->Aspirante_model->Update($idAspirantes,$data)) {
			redirect(base_url()."Aspirantes/Aspirantes");
		}else{
			$this->session->ser_flashdata("error","no se actualizaron los campos");
				redirect(base_url()."Aspirantes/Aspirantes/Add");
		}
	}

	public function Delete($idAspirantes){
		$data  =array(
			'Aceptados' => "0",
		);
		$this->Aspirante_model->Update($idAspirantes,$data);
		
		redirect(base_url()."Aspirantes/Aspirantes");
	}
}
