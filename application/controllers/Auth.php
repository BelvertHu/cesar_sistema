<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Usuario_model");
	}
	public function index()
	{
		if ($this->session->userdata("Login")) {
			redirect(base_url()."Dashboard");
		}
		else{
			$this->load->view("Admin/Login");
		}
		

	}

	public function Login(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$res = $this->Usuario_model->Login($username, sha1($password));

		if (!$res) {
			$this->session->set_flashdata("error","El usuario y/o contraseña son incorrectos");
			redirect(base_url());
		}
		else{
			$data  = array(
				'id' => $res->idusuarios, 
				'nombre' => $res->nombre,
				'rol' => $res->rol_id,
				'login' => TRUE
			);
			$this->session->set_userdata($data);
			redirect(base_url()."Dashboard");
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
