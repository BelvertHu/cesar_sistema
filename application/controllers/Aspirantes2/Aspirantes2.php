<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspirantes2 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Aspirante2_model");
	}

	public function Index()
	{
		$data = array(
			'aspirante2' => $this->Aspirante2_model->getregistro()
			,);
		
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes2/Registrar",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Add()
	{
		
        $this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes2/Add");
		$this->load->view("Layouts/Footer");
	}

	public function Store(){
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoM");
		$CarreraE = $this ->input->post("CarreraE");
		$Adeudo = $this ->input->post("Adeudo");
		$Respuesta = $this ->input->post("Respuesta");
		$Padeces = $this ->input->post("Padeces");
		$Respuesta2 = $this ->input->post("Respuesta2");
		$Alergias = $this ->input->post("Alergias");
        $Respuesta3 = $this ->input->post("Respuesta3");
        $ansiedad = $this ->input->post("ansiedad");
        $Sufres = $this ->input->post("Sufres");
        $deprecion = $this ->input->post("deprecion");
        $Rechazado = $this ->input->post("Rechazado");
        $Adaptas = $this ->input->post("Adaptas");
        $Otra = $this ->input->post("Otra");
        
		
		
		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'CarreraE' => $CarreraE,
			'Adeudo' => $Adeudo,
			'Respuesta' => $Respuesta,
			'Padeces' => $Padeces,
			'Respuesta2' => $Respuesta2,
            'Alergias' => $Alergias,
            'Respuesta3' => $Respuesta3,
            'ansiedad' => $ansiedad,
            'Sufres' => $Sufres,
            'deprecion' => $deprecion,
            'Rechazado' => $Rechazado,
            'Adaptas' => $Adaptas,
            'Otra' => $Otra,
            'Estado' => "1"
		);

	if ($this->Aspirante2_model->save($data)) {
		redirect(base_url()."Aspirantes2/Aspirantes2");

	}else
	$this->session->ser_flashdata("error","no se relizaron registros");
	redirect(base_url()."Aspirantes2/Aspirantes2/Add");

	}

	public function Edit($id){

		$data = array(
			'aspirante2' => $this->Aspirante2_model->getregistros($id),);
		$this->load->view("Layouts/Header");
		$this->load->view("Layouts/Aside");
		$this->load->view("Admin/Aspirantes2/Edit",$data);
		$this->load->view("Layouts/Footer");
	}

	public function Update(){
		$id= $this ->input->post("id");
		$Nombre = $this ->input->post("Nombre");
		$ApellidoP = $this ->input->post("ApellidoP");
		$ApellidoM = $this ->input->post("ApellidoM");
		$CarreraE = $this ->input->post("CarreraE");
		$Adeudo = $this ->input->post("Adeudo");
		$Respuesta = $this ->input->post("Respuesta");
		$Padeces = $this ->input->post("Padeces");
		$Respuesta2 = $this ->input->post("Respuesta2");
		$Alergias = $this ->input->post("Alergias");
        $Respuesta3 = $this ->input->post("Respuesta3");
        $ansiedad = $this ->input->post("ansiedad");
        $Sufres = $this ->input->post("Sufres");
        $deprecion = $this ->input->post("deprecion");
        $Rechazado = $this ->input->post("Rechazado");
        $Adaptas = $this ->input->post("Adaptas");
        $Otra = $this ->input->post("Otra");

		$data  =array(
			'Nombre' => $Nombre,
			'ApellidoP' => $ApellidoP,
			'ApellidoM' => $ApellidoM,
			'CarreraE' => $CarreraE,
			'Adeudo' => $Adeudo,
			'Respuesta' => $Respuesta,
			'Padeces' => $Padeces,
			'Respuesta2' => $Respuesta2,
            'Alergias' => $Alergias,
            'Respuesta3' => $Respuesta3,
            'ansiedad' => $ansiedad,
            'Sufres' => $Sufres,
            'deprecion' => $deprecion,
            'Rechazado' => $Rechazado,
            'Adaptas' => $Adaptas,
            'Otra' => $Otra
		);
		if ($this->Aspirante2_model->Update($id,$data)) {
			redirect(base_url()."Aspirantes2/Aspirantes2");
		}else{
			$this->session->ser_flashdata("error","no se actualizaron los campos");
				redirect(base_url()."Aspirantes2/Aspirantes2/Add");
		}
	}

	public function Delete($id){
		$data  =array(
			'Estado' => "0",
		);
		$this->Aspirante2_model->Update($id,$data);
		
		redirect(base_url()."Aspirantes2/Aspirantes2");
	}
}
