<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Captura_model extends CI_Model {

    public function getregistro(){
        $this->db->where("Estado","1");
        $resultados = $this->db->get("capturacali");
        return $resultados->result();
         
    }

    public function save ($data){
        return $this->db->Insert("capturacali",$data);

    }

    public function getregistros ($id){
        $this->db->where("id",$id);
        $resultado = $this->db->get("capturacali");
        return $resultado->row();
    }

    public function Update ($id,$data){
        $this->db->where("id",$id);
        return $this->db->Update("capturacali",$data);
        
    }

}