<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspirante_model extends CI_Model {

    public function getregistro(){
        $this->db->where("Aceptados","1");
        $resultados = $this->db->get("aspirantes");
        return $resultados->result();
         
    }

    public function Save ($data){
        return $this->db->Insert("aspirantes",$data);

    }

    public function getregistros ($idAspirantes){
        $this->db->where("idAspirantes",$idAspirantes);
        $resultado = $this->db->get("aspirantes");
        return $resultado->row();
    }

    public function Update ($idAspirantes,$data){
        $this->db->where("idAspirantes",$idAspirantes);
        return $this->db->Update("aspirantes",$data);
        
    }

}