<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subirimagen_model extends CI_Model {


    public function construct() {
        parent::__construct();
    }
    
    function Subir($Nombreclave,$Actan,$Curp,$ComprobanteD,$ComprobanteE,$Fotografia,$FotografiaAd,$FotoINS)
    {
        $data = array(
            'NombreClave' => $Nombreclave,
            'Actan' => $Actan,
            'Curp' => $Curp,
            'ComprobanteD' => $ComprobanteD,
            'ComprobanteE' => $ComprobanteE,
            'Fotografia' => $Fotografia,
            'FotografiaAd' => $FotografiaAd,
            'FotoINS' => $FotoINS

        );
        return $this->db->Insert('imagenes', $data);
    }
}