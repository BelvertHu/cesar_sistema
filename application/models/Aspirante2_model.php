<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspirante2_model extends CI_Model {

    public function getregistro(){
        $this->db->where("Estado","1");
        $resultados = $this->db->get("aspirante2");
        return $resultados->result();
         
    }

    public function Save ($data){
        return $this->db->Insert("aspirante2",$data);

    }

    public function getregistros ($id){
        $this->db->where("id",$id);
        $resultado = $this->db->get("aspirante2");
        return $resultado->row();
    }

    public function Update ($id,$data){
        $this->db->where("id",$id);
        return $this->db->Update("aspirante2",$data);
        
    }

}