<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registro_model extends CI_Model {

    public function getregistro(){
        $this->db->where("estado","1");
        $resultados = $this->db->get("persona_docentes");
        return $resultados->result();
         
    }

    public function save ($data){
        return $this->db->Insert("persona_docentes",$data);

    }

    public function getregistros ($id_persona_docente){
        $this->db->where("id_persona_docente",$id_persona_docente);
        $resultado = $this->db->get("persona_docentes");
        return $resultado->row();
    }

    public function Update ($id_persona_docente,$data){
        $this->db->where("id_persona_docente",$id_persona_docente);
        return $this->db->Update("persona_docentes",$data);
        
    }

}