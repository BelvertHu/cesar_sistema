<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <h3 class="title-5 m-b-35">data table</h3>
                                <!-- aparto de opciones -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a href="<?php echo base_url();?>Aspirantes/Aspirantes/Add" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Registrar Aspirantes</a>

                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <a href="<?php echo base_url();?>Dashboard" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i></i>Regresar</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- cierra aparto de opciones -->
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Apellido Paterno</th>
                                                <th>Apellido Materno</th>
                                                <th>Curp</th>
                                                <th>Genero</th>
                                                <th>Estado</th>
                                                <th>Municipio</th>
                                                <th>Localidad</th>
                                                <th>Cp</th>
                                                <th>Telefono</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php if(!empty($aspirantes)):?>
                                                <?php foreach($aspirantes as $aspirantes):?>
                                                    <tr class="tr-shadow">
                                                    <td><?php echo $aspirantes->idAspirantes;?> </td>
                                                        <td><?php echo $aspirantes->Nombre;?></td>
                                                        <td><?php echo $aspirantes->ApellidoP;?></td>
                                                        <td><?php echo $aspirantes->ApellidoM;?></td>
                                                        <td><?php echo $aspirantes->curp;?></td>
                                                        <td><?php echo $aspirantes->sexo;?></td>
                                                        <td><?php echo $aspirantes->estado;?></td>
                                                        <td><?php echo $aspirantes->municipio;?></td>
                                                        <td><?php echo $aspirantes->localidad;?></td>
                                                        <td><?php echo $aspirantes->CP;?></td>
                                                        <td><?php echo $aspirantes->Telefefono;?></td>
                                                        <td>
                                                            <div class="table-data-feature">
                                                                
                                                                <button class="btn btn-success">
                                                                    <a href="<?php echo base_url();?>Aspirantes/Aspirantes/Edit/<?php echo $aspirantes->idAspirantes;?>" class="zmdi zmdi-edit"></a>
                                                                </button>
                                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal">
                                                                <a href="<?php echo base_url();?>Aspirantes/Aspirantes/Delete/<?php 
                                                                echo $aspirantes->idAspirantes;?>" 
                                                                class="zmdi zmdi-delete btn-remove"> <span class="btn-remove"></span></a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div> 
            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
        
</div>