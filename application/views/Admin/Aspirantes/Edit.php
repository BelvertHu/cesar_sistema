<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

            <div class="card">
                                    <div class="card-header">
                                        Registrar
                                        <strong>Aspirantes</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        
                                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("error")?></p>
                            </div>
                        <?php endif; ?>
                                        <form action="<?php echo base_url();?>Aspirantes/Aspirantes/Update" method="post" class="form-horizontal">
                                            <input type="Hiden" value="<?php  echo $aspirantes->idAspirantes;?>" name="idAspirantes">
                              
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-small" class=" form-control-label">Nombre</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-small" name="Nombre" value="<?php  echo $aspirantes->Nombre ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Apellido Paterno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="ApellidoP" value="<?php  echo $aspirantes->ApellidoP ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Apellido Materno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="ApellidoM" value="<?php  echo $aspirantes->ApellidoM ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Fecha de Nacimiento </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="FNacimiento" value="<?php  echo $aspirantes->FNacimiento ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Curp</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="curp" value="<?php  echo $aspirantes->curp ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Genero</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="sexo" value="<?php  echo $aspirantes->sexo ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Estado</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="estado" value="<?php  echo $aspirantes->estado ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Municipio</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="municipio" value="<?php  echo $aspirantes->municipio ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Localidad</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="localidad" value="<?php  echo $aspirantes->localidad ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Cp</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="CP" value="<?php  echo $aspirantes->CP ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Numero de Domicilio</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="NumeroDomicilio" value="<?php  echo $aspirantes->NumeroDomicilio ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Telefono</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Telefefono" value="<?php  echo $aspirantes->Telefefono ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Telefono 2 </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Telefono2" value="<?php  echo $aspirantes->Telefono2 ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Codigo</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="codigo" value="<?php  echo $aspirantes->codigo ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Mostra 1 no Mostrar 0 </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Aceptados" value="<?php  echo $aspirantes->Aceptados ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Guardar</button>
                                        
                                    </div>
                                    </form>
                                    
                </div>

            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
</div>