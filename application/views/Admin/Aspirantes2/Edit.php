<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

            <div class="card">
                                    <div class="card-header">
                                        Editar
                                        <strong>Aspirantes</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        
                                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("error")?></p>
                            </div>
                        <?php endif; ?>
                                        <form action="<?php echo base_url();?>Aspirantes2/Aspirantes2/Update" method="post" class="form-horizontal">
                                            <input type="Hiden" value="<?php  echo $aspirante2->id;?>" name="id">
                              
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-small" class=" form-control-label">Nombre</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-small" name="Nombre" value="<?php  echo $aspirante2->Nombre ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Apellido Paterno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="ApellidoP" value="<?php  echo $aspirante2->ApellidoP ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Apellido Materno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="ApellidoM" value="<?php  echo $aspirante2->ApellidoM ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Carrera Solicitadad</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="CarreraE" value="<?php  echo $aspirante2->CarreraE ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Adeudas Materias en la Secundaria</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Adeudo" value="<?php  echo $aspirante2->Adeudo ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si dijiste que Si Cuantas</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta" value="<?php  echo $aspirante2->Respuesta ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Padeces alguna enfermedad SI/NO</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Padeces" value="<?php  echo $aspirante2->Padeces ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si tu respuesta es SI, de que tipo y cuál es el tratamiento que tienes </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta2" value="<?php  echo $aspirante2->Respuesta2 ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Tienes alergias SI/NO</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Alergias" value="<?php  echo $aspirante2->Alergias ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si tu respuesta es SI de que tipo </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta2" value="<?php  echo $aspirante2->Respuesta2 ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de ansiedad</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="ansiedad" value="<?php  echo $aspirante2->ansiedad ?>"class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de estrés</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Sufres" value="<?php  echo $aspirante2->Sufres ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de depresión </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="deprecion" value="<?php  echo $aspirante2->deprecion ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Te sientes rechazado por tu familia o tus amigos </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Rechazado" value="<?php  echo $aspirante2->Rechazado ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">No te adaptas fácilmente a cambios </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Adaptas" value="<?php  echo $aspirante2->Adaptas ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Alguna otra situación emocional que no haya sido considerada anteriormente</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Otra" value="<?php  echo $aspirante2->Otra ?>" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                   
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Guardar</button>
                                        
                                    </div>
                                    </form>
                                    
                </div>

            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
</div>