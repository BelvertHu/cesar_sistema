<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

            <div class="card">
                                    <div class="card-header">
                                        Registrar
                                        <strong>Aspirantes</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        
                                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("error")?></p>
                            </div>
                        <?php endif; ?>
                                        <form action="<?php echo base_url();?>Aspirantes2/Aspirantes2/Store" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-small" class=" form-control-label">Nombre</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-small" name="Nombre" placeholder="Nombre" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Apellido Paterno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="ApellidoP" placeholder="Apellido Paterno" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Apellido Materno</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="ApellidoM" placeholder="Apellido Materno" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Carrera Solicitadad</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="CarreraE" placeholder="Carrera Solicitadad" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Adeudas Materias en la Secundaria</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Adeudo" placeholder="Si/No" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si dijiste que Si Cuantas</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta" placeholder="Cuantas Adeudad" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Padeces alguna enfermedad SI/NO</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Padeces" placeholder="Si/No" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si tu respuesta es SI, de que tipo y cuál es el tratamiento que tienes </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta2" placeholder="Respuesta" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Tienes alergias SI/NO</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Alergias" placeholder="SI/NO" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Si tu respuesta es SI de que tipo </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Respuesta2" placeholder="Si tu respuesta es SI de que tipo " class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de ansiedad</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="ansiedad" placeholder="SI/NO" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de estrés</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Sufres" placeholder="SI/NO" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Sufres de depresión </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="deprecion" placeholder="SI/NO " class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Te sientes rechazado por tu familia o tus amigos </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Rechazado" placeholder="No te adaptas fácilmente a cambios " class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">No te adaptas fácilmente a cambios </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Adaptas" placeholder="No te adaptas fácilmente a cambios " class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Alguna otra situación emocional que no haya sido considerada anteriormente</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Otra" placeholder="Describe" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            
                                        
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Guardar</button>
                                 
                                    </form>
                                    <button type="button" class="btn btn-danger btn-lg btn-block"><a href="<?php echo base_url();?>Dashboard" >Regresar</a></button>

                                    
                </div>

            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
</div>