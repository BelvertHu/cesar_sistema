<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <h3 class="title-5 m-b-35">Listar Aspirantes </h3>
                                <!-- aparto de opciones -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a href="<?php echo base_url();?>Aspirantes2/Aspirantes2/Add" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Registrar Aspirantes</a>

                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <a href="<?php echo base_url();?>dashboard" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i></i>Regresar</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- cierra aparto de opciones -->
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Apellido Paterno</th>
                                                <th>Apellido Materno</th>
                                                <th>Carreara</th>
                                                <th>Adeudad Materias</th>
                                                <th>Padeces</th>
                                                <th>Alergias</th>
                                                <th>Ansiedad</th>
                                                <th>Estres</th>
                                                <th>Depresion</th>
                                                <th>Rechazo</th>
                                                <th>Te adaptas</th>
                                                <th>Otra</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php if(!empty($aspirante2)):?>
                                                <?php foreach($aspirante2 as $aspirante2):?>
                                                    <tr class="tr-shadow">
                                                    <td><?php echo $aspirante2->id;?> </td>
                                                        <td><?php echo $aspirante2->Nombre;?></td>
                                                        <td><?php echo $aspirante2->ApellidoP;?></td>
                                                        <td><?php echo $aspirante2->ApellidoM;?></td>
                                                        <td><?php echo $aspirante2->CarreraE;?></td>
                                                        <td><?php echo $aspirante2->Adeudo;?></td>
                                                        <td><?php echo $aspirante2->Padeces;?></td>
                                                        <td><?php echo $aspirante2->Alergias;?></td>
                                                        <td><?php echo $aspirante2->ansiedad;?></td>
                                                        <td><?php echo $aspirante2->Sufres;?></td>
                                                        <td><?php echo $aspirante2->deprecion;?></td>
                                                        <td><?php echo $aspirante2->Rechazado;?></td>
                                                        <td><?php echo $aspirante2->Adaptas;?></td>
                                                        <td><?php echo $aspirante2->Otra;?></td>

                                                        <td>
                                                            <div class="table-data-feature">
                                                                
                                                                <button class="btn btn-success">
                                                                    <a href="<?php echo base_url();?>Aspirantes2/Aspirantes2/Edit/<?php echo $aspirante2->id;?>" class="zmdi zmdi-edit"></a>
                                                                </button>
                                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal">
                                                                <a href="<?php echo base_url();?>Aspirantes2/Aspirantes2/Delete/<?php 
                                                                echo $aspirante2->id;?>" 
                                                                class="zmdi zmdi-delete btn-remove"> <span class="btn-remove"></span></a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div> 
            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
        
</div>