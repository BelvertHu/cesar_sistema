<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

            <div class="card">
                                    <div class="card-header">
                                        Registrar
                                        <strong>Aspirantes</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        
                                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("error")?></p>
                            </div>
                        <?php endif; ?>
                                        <form action="<?php echo base_url();?>Captura/Captura/Store" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-small" class=" form-control-label">Matricula</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-small" name="Matricula" placeholder="Matricula" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Nombre</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="Nombre" placeholder="Nombre Completo" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Parcial 1</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-normal" name="Parcial1" placeholder="Parcial 1" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 2</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial2" placeholder="Parcial 2" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 3</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial3" placeholder="Parcial 3" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 4</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial4" placeholder="Parcial 4" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 5</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial5" placeholder="Parcial 5" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 6</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial6" placeholder="Parcial 6" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 7</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial7" placeholder="Parcial 7" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 8</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial8" placeholder="Parcial 8" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Parcial 9</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-large" name="Parcial9" placeholder="Parcial 9" class="input-sm form-control-sm form-control">
                                                </div>
                                            
                                                
                                            
                                        
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Guardar</button>
                                 
                                    </form>
                                    <button type="button" class="btn btn-danger btn-lg btn-block"><a href="<?php echo base_url();?>Dashboard" >Regresar</a></button>

                                    
                </div>

            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
</div>