<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <h3 class="title-5 m-b-35">Listar calificaciones </h3>
                                <!-- aparto de opciones -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a href="<?php echo base_url();?>Captura/Captura/Add" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Registrar Aspirantes</a>

                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <a href="<?php echo base_url();?>Dashboard" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i></i>Regresar</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- cierra aparto de opciones -->
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Matricula</th>
                                                <th>Nombre</th>
                                                <th>Parcial 1</th>
                                                <th>Parcial 2</th>
                                                <th>Parcial 3</th>
                                                <th>Parcial 4</th>
                                                <th>Parcial 5</th>
                                                <th>Parcial 6</th>
                                                <th>Parcial 7</th>
                                                <th>Parcial 8</th>
                                                <th>Parcial 9</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php if(!empty($capturacali)):?>
                                                <?php foreach($capturacali as $capturacali):?>
                                                    <tr class="tr-shadow">
                                                    <td><?php echo $capturacali->id;?> </td>
                                                        <td><?php echo $capturacali->Matricula;?></td>
                                                        <td><?php echo $capturacali->Nombre;?></td>
                                                        <td><?php echo $capturacali->Parcial1;?></td>
                                                        <td><?php echo $capturacali->Parcial2;?></td>
                                                        <td><?php echo $capturacali->Parcial3;?></td>
                                                        <td><?php echo $capturacali->Parcial4;?></td>
                                                        <td><?php echo $capturacali->Parcial5;?></td>
                                                        <td><?php echo $capturacali->Parcial6;?></td>
                                                        <td><?php echo $capturacali->Parcial7;?></td>
                                                        <td><?php echo $capturacali->Parcial8;?></td>
                                                        <td><?php echo $capturacali->Parcial9;?></td>
                                                    

                                                        <td>
                                                            <div class="table-data-feature">
                                                                
                                                                <button class="btn btn-success">
                                                                    <a href="<?php echo base_url();?>Captura/Captura/Edit/<?php echo $capturacali->id;?>" class="zmdi zmdi-edit"></a>
                                                                </button>
                                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal">
                                                                <a href="<?php echo base_url();?>Captura/Captura/Delete/<?php 
                                                                echo $capturacali->id;?>" 
                                                                class="zmdi zmdi-delete btn-remove"> <span class="btn-remove"></span></a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div> 
            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
        
</div>