<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

            <div class="card">
                                    <div class="card-header">
                                        Registrar
                                        <strong>Documentos</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        
                                    <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("error")?></p>
                            </div>
                        <?php endif; ?>
                                        <form action="<?php echo base_url();?>Imagen/Imagen/SubirImagen" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-small" class=" form-control-label">Nombre + Clave </label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="text" id="input-small" name="NombreClave" placeholder="Nombre + clave" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-normal" class=" form-control-label">Acta de nacimiento</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-normal" name="Actan"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Curp</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="Curp"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Comprobante De Domicilio</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="ComprobanteD"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Comprobante De Estudios</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="ComprobanteE"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Fotografia del Aspirante</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="Fotografia"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Fotografia de la ficha de Admision</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="FotografiaAd"  class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-sm-5">
                                                    <label for="input-large" class=" form-control-label">Fotografia del formato de Inscripcion</label>
                                                </div>
                                                <div class="col col-sm-6">
                                                    <input type="file" id="input-large" name="FotoIns" placeholder=" H / M" class="input-sm form-control-sm form-control">
                                                </div>
                                            </div>
                                            
                                            
                                            
                                        
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Guardar</button>
                                 
                                    </form>
                                    <button type="button" class="btn btn-danger btn-lg btn-block"><a href="<?php echo base_url();?>Dashboard" >Regresar</a></button>

                                    
                </div>

            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
</div>