<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <h3 class="title-5 m-b-35">Registrar Profesores</h3>
                                <!-- aparto de opciones -->
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a href="<?php echo base_url();?>Registros/Registros/Add" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Registrar profesores</a>

                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <a href="<?php echo base_url();?>Dashboard" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i></i>Regresar</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- cierra aparto de opciones -->
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Apellido Paterno</th>
                                                <th>Apellido Materno</th>
                                                <th>Ciudad</th>
                                                <th>Dirección</th>
                                                <th>Telefono</th>
                                                <th>Genero</th>
                                                <th>Matricula</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php if(!empty($persona_docentes)):?>
                                                <?php foreach($persona_docentes as $persona_docentes):?>
                                                    <tr class="tr-shadow">
                                                    <td><?php echo $persona_docentes->id_persona_docente;?> </td>
                                                        <td><?php echo $persona_docentes->Nombre;?></td>
                                                        <td><?php echo $persona_docentes->ApellidoP;?></td>
                                                        <td><?php echo $persona_docentes->ApellidoM;?></td>
                                                        <td><?php echo $persona_docentes->Ciudad;?></td>
                                                        <td><?php echo $persona_docentes->direccion;?></td>
                                                        <td><?php echo $persona_docentes->telefono;?></td>
                                                        <td><?php echo $persona_docentes->sexo;?></td>
                                                        <td><?php echo $persona_docentes->matricula;?></td>
                                                        <td>
                                                            <div class="table-data-feature">
                                                                
                                                                <button class="btn btn-success">
                                                                    <a href="<?php echo base_url();?>Registros/Registros/Edit/<?php echo $persona_docentes->id_persona_docente;?>" class="zmdi zmdi-edit"></a>
                                                                </button>
                                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal">
                                                                <a href="<?php echo base_url();?>Registros/Registros/Delete/<?php 
                                                                echo $persona_docentes->id_persona_docente;?>" 
                                                                class="zmdi zmdi-delete btn-remove"> <span class="btn-remove"></span></a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div> 
            </div>                        
        </div>
    </div>
        <!-- END DATA TABLE -->
        
</div>