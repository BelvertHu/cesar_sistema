<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?php echo base_url();?>Dashboard">
                    <img src="<?php echo base_url();?>images/icon/logo-blue.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-plus-square"></i>Listas <i class="fa fa-angle-double-down"></i></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url();?>Registros/Registros">listar Docentes</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>Aspirantes2/Aspirantes2">Listar Aspirantes </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>Captura/Captura">Listar calificaciones</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-align-justify"></i>Registrar <i class="fa fa-angle-double-down"></i></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url();?>Registros/Registros/Add">Registrar Docentes</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>Aspirantes2/Aspirantes2/Add">Registrar Aspirantes </a>
                                </li>
                                <li>
                                <a href="<?php echo base_url();?>Captura/Captura/Add">Registrar Calificaciones</a>
                                </li>
                                
                            </ul>
                        </li>
                        
                        <li>
                            <a href="<?php echo base_url();?>Imagen/Imagen/Subirimagen">
                                <i class="fas fa-table"></i>Subir Documentos</a>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-plus-square"></i>Listar 2 <i class="fa fa-angle-double-down"></i></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url();?>Registros/Registros">listar Docentes</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>Aspirantes/Aspirantes">Listar Aceptados </a>
                                </li>
                                <li>
                                <a href="<?php echo base_url();?>Aspirantes/Apirantes/Add">Registrar Aspirantes </a>
                                </li>
                                
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
    

        